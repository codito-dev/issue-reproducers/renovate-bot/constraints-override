# Renovate Bot: constraints override

Issue reproducer for [this problem](https://github.com/renovatebot/renovate/discussions/21204#discussioncomment-6345792):

In our case overriding constraints does not work even with `force`. We have:

- `"extractedConstraints": {"php": "8.2.*"}` from `composer.json`
- `"force": {"constraints": {"php": "7.4"}}` from `renovate.json` (I see it in "migrated config" section of DEBUG
  output)
- packages still are updated on PHP 8.2 runtime and bumped to ones not compatible with 7.4

The reason why we want to do that is that we're migrating our app, we have 8.2 on production, but some of the machines still needs migration (code is compatible with both 7.4 and 8.2 thanks to PHPCompatibility sniffs). We want to use 8.2 for local development (only as runtime, without using features at this point), but disallow Renovate from bumping packages to versions not compatible with 7.4.
