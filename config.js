module.exports = {
    semanticCommitType: 'fix',
    packageRules: [
        {
            matchPackagePatterns: ['^illuminate/'],
            groupName: 'illuminate packages',
            groupSlug: 'illuminate',
        },
        {
            matchPackagePatterns: ['^symfony/'],
            groupName: 'symfony packages',
            groupSlug: 'symfony',
        },
        {
            matchDepTypes: ['require-dev'],
            semanticCommitType: 'chore',
            prPriority: -5,
        },
    ],
};
